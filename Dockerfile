FROM maven:3-jdk-11 AS build
COPY src /project/src
COPY pom.xml /project
RUN mvn -f /project/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /project/target/test_ci-cd-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]